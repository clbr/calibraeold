# Calibrae

A clone of the steem blockchain system, reimplemented in Golang, based on [SporeDB](https://gitlab.com/Sporedb/sporedb). See the original Sporedb readme text [in the section below](#sporedb-).

Calibrae builds upon the things in steem that work, and improves elements that have proven susceptible to abuse:

- The pegged SBD type token is eliminated because it is both overcomplex for the token issuance rate control as well as difficult to understand for the user. In the future such instruments may be added or integrated from external networks via inter-chain decentralised exchange protocols, should compelling arguments for such additions be presented and agreed upon.

- Enforcing bandwidth limits that correspond to human activity, and not so much that it becomes possible to spam transactions into the database using bot scripts.

- Stake, the vote power attribute of the account, has a 1% per day drawdown rate, meaning effectively the same as the original 2 year drawdown rate of Steem, but on an otherwise unchanging stake, more than half of the remaining will draw out in about 3 months. 

- All rewards are paid directly into Stake, to limit the liquidity of accounts in order to reduce the volatility of the exchange market in the tokens. Proportionally, the amount of volatility will be greatly biased towards holders versus speculators. This benefits long term investment in the platform as well as eliminating some of the complexity of the rewards calculation system.

- Refining the reputation system, to eliminate the ability to abuse the voting system, balancing reputation against stake, adding a reputation reduction when users ignore (shun) bad behaving accounts, applied entirely with comparisons of reputation scores and an inverse square sum so that many accounts doing this do not attain more power than brings the reputation to zero, relative to the account's reputation . Thus, a non-posting user's power can be restricted, this is an exploitable mechanism with Steem. There will be no such thing as negative reputation, if the resultant reputation is under zero, it is the same as zero. Zero reputation will restrict the account to one transaction per day, and limit the amount transferrable to 10% of remaining, meaning up to two weeks if the account holder does not wish to make amends and simply wishes to liquidate and leave.

- Vote power is calculated by the combination of the account's current reputation, divided by the maximum reputation score on the network, and multiplied by Stake, and this amount fills back up at a rate that allows the total amount to be used during a time period of one day, similar to a reservoir of fuel, and the user will see this information clearly in the interface, rather than arbitrarily dividing 100% into an arbitrary curve, that is not visible to the user. The user will see they have some number of vote power, and can then arbitrarily select a percentage of available VP to apply to a vote, with both a slider and a numeric field.

- The forum post data is stored as simple files named by their hashes, rather than cluttering up the backend database, as posts are more suited to being stored in files anyway, for performance in retrieval, and also placing the most recently written posts in the server's disk cache, where they will be frequently read back. In the event that the volume of posts starts to impact node operators ability to store the database, they can set limits on the cache size and nodes that offer the complete database will be referred to in order to retrieve the old data, which will be noted in an identifier associated with the operator's account and the node public key and IP address in the cluster membership database.

- Posts will also have the option of being 'novated', reposted, but with the old data, in a new post transaction, to enable users to extend their rewards on work that they think deserves repeat showings. 

- The post data store will work on a basis of storing the hash of the body of the post. Thus if an identical post is made again, it only has to be stored once, and simply has multiple references. The data itself is immutable, changes are added as new posts containing a diff of the changes, instead of changing the original data. To minimise copy and past spammy posting, a rule can be made that the exact same body cannot have a new reference made to it from the same account more than once in ten minutes. This will stop the flood of 'nice post' comments that plagues Steem.

- There will be a special type of account, that is a child account of a regular account, which is the account owner. These accounts can have hierarchies of other accounts that have various privileges to post, and this will form the basis of the cognate of subreddits and 'communities'. Amongst other functions, the owners and membership administrators will be able to ban accounts from posting on these subgroups.

- Calibrae nodes serve up the web application directly, and use the beneficiary system to fund the nodes, with mechanisms in place to combat (malicious) alteration of the app enforced by other nodes checking it via Tor, and logging their reports on the chain. Refusing to serve the app will also count as a failure report, with the distinction noted in the report - unavailable nodes are equally undesirable in a live list as malicious ones. These will be stored in a permanent record as log entries attached to accounts in the cluster membership database. 

- To enable other interfaces, the user of such will either use the server node itself directly, or nodes can offer a ledger-only interface that only exposes the ledger to these lower data payload sized transactions. It would make sense to make a caching server library which simply stores data that has been requested from the network, so that it can be retrieved offline, or to avoid repeated requests for that data, and conserve bandwidth and save time for the client.

- Rather than depend on electronic methods for key storage, users will as well as their standard WIF format keys have two word-based keys they can store on paper or in memory more readily than the 32 character WIF keys.

- Because the base of this platform is SporeDB, a lot of changes must also necessarily go along with this - the transaction clearance latency is far lower, and completely dynamic (it will usually be under 1 second):

  - There is no witness election or witness schedule, instead the primary means of funding a node is to run the web application and configure a beneficiary rate;

  - As a mechanism to stop double spends on the network, there will be a 5 minute mutex lockout so that transactions will simply not be accepted if there was transactions made through another node less than 5 minutes before. The capacity of nodes to handle transactions is far more than even a corporate account could need, and generally such a busy account would be running its own dedicated node for its transaction processing.

  - For critical, financial transactions in the ledger, alongside the standard single key where an account balance is stored, is a transaction history on each account, which permits auditing.



## Getting the sources, the easy way

You will need Golang installed, [see below for information about this](#recommendations-for-aspiring-developers).

```
go get gitlab.com/calibr.ae/calibrae
```

If you prefer to use the faster SSH git clone method, you will want to do this instead:

```
cd $GOPATH/src
mkdir -p gitlab.com/calibrae-project
cd gitlab.com/calibrae-project
git clone git@gitlab.com:calibrae-project/calibrae.git
```

## Build instructions

**[See the section below](#recommendations-for-aspiring-developers) for recommendations for aspiring developers, to set up your build environment before trying these.** 

Many golang repositories do not include these basic instructions because they assume developers have already learned them. We do not make this assumption, we want you to get a taste for it first, then you will see why you need to Go learn more.

To learn more about how you develop with Golang, visit [https://golang.org/doc/install](https://golang.org/doc/install)

### Preparing your workspace

It is not simple to find the answer to this simple question out there on teh interwebz, but when you have external dependencies pulled in via github or similar, you can automate getting the dependencies for everything this way:

```
cd $GOPATH/src
go get ./...
```

> Note that the Makefile in the root of the repository executes this command, prior to a `go install` command, which places the produced binary in your `$GOPATH/bin` folder, should you not have `make` installed, you can use the method above.

This automatically scans the import blocks of every go source in your entire workspace, `go get`s all the necessaries, and then you can `go install path/to/cmd/programname` and voila! The `...` means 'recursively scan all sources and all dependencies'.

When you are adding new dependencies, you will need to `go get` it, you can just rerun over your whole workspace again as described above, after you add the new dependency in a source file's import block.

### How to build

The following commands will build and install into your $GOPATH/bin folder:

```
go install gitlab.com/calibr.ae/calibrae/cmd/calibrd
```


### Building locally from your copy of the repository

From within the calibrae/ directory, issue the following command to build and install the binary into your $GOPATH/bin folder (see below recommendations for how to put this in your path on linux):

```
go install ./cmd/<commandname>
```

where &lt;commandname&gt; is one of the subfolders of [calibrae/cmd](calibrae/cmd)

## Commandline parameters standard

All commandline executables in this repository have a standard based on the Golang 'flags' package. We use the extended version from here: [https://github.com/namsral/flag](https://github.com/namsral/flag), which adds the transparent ability to use configuration files and environment variables as well, which is more integrated than the base golang standard.

To get the help information for parameters:

```
<commandname> -h
```

Where there is default options using a configuration file and work directory, commandline flags can be used to set parameters that will override parameters in the directories.

# Recommendations for aspiring developers

Because Calibrae is written in Golang, it is simple for anyone to get involved without learning a new operating system, and the development environment is very simple to understand, our other developers will happily help you, if the instructions below are not sufficient to get you started.

Linux, specifically Arch Linux or a derivative, is highly recommended, but all of the tools listed here are available as binaries for all platforms.

## Golang

You can download Go from here: [https://golang.org/dl/](https://golang.org/dl/)

On linux, it is highly recommended you add the following to your shell init script (for most, this will be `$HOME/.bashrc`):

```
export GOPATH=$HOME
export PATH=$HOME/bin:$PATH
export CDPATH=$GOPATH/src/github.com:$GOPATH/src/gitlab.com:$GOPATH/src/github.com
```

The last command for CDPATH means you can now `cd calibr.ae/calibrae` and instantly be in the root of this repository.

***TODO: Add the relevant instructions for windows, MacOS should be the same since it is based on BSD unix***

This will put all the binaries in the bin/ directory of your profile folder, and set it as part of the command search path, meaning you can invoke generated binaries without specifying path. 

The source files that are downloaded by go to build the binaries will be available in the src/ directory of your home folder, in a folder hierarchy based on the server names and group names of the repositories.

## Integrated Development Environment

Visual Studio Code is recommended for developing Calibrae. 

Download it here: [https://code.visualstudio.com/download](https://code.visualstudio.com/download). 

When using the above recommendation for the GOPATH, you also need to tell VSCode to use the same, for some reason it does not read it out of .bashrc, so add this line to your settings (File->Prefecences->Settings)

```
"go.gopath": "<what you set your GOPATH as>"
```

Note that `$HOME` in the configuration will flag an error. Instead, set it to `/home/<username>` which you can get by opening the integrated terminal and entering

```
echo $HOME
```

Doing this will have everything neatly in one place, even if it doesn't seem to be default for Go presently, it really should be.

For Arch linux users ([Antergos Linux](https://antergos.com/) is very easy to install, based on Arch) install pacaur: 

```
sudo pacman -S pacaur
```

then install VSCode from AUR:

```
pacaur -S visual-studio-code
```

## Git Repository Management

[GitKraken](https://code.visualstudio.com/download) is a very easy to use tool for managing git repositories. VSCode has basic Git functions built in, but for when more complex tasks are required, GitKraken lets you do it with a mouse.

---

# SporeDB [![build status](https://gitlab.com/SporeDB/sporedb/badges/master/build.svg)](https://gitlab.com/SporeDB/sporedb/commits/master)

SporeDB is a work-in-progress to a highly scalable, fast, resilient, decentralized and flexible database engine, named by analogy with the Mycology science.

## Idea

*Extract from the [full whitepaper](https://static.lesterpig.com/sporedb.pdf)*

Distributed databases are very popular when it comes to service scalability and high availability.
Such databases, like Apache Cassandra, MongoDB or Redis are able to handle node or network failures, but cannot handle nodes acting in a byzantine way.

Solving the Byzantine problem usually involves complex, costly or non-scalable consensus algorithms.
We can mention the well-known PBFT, the Bitcoin Proof-of-Work , the Tendermint protocol or the Stellar CP among many others.
Generally, these protocols require some strong coordination between nodes (leadership for example), or are mostly designed for a specific application (crypto-currencies for example).
This strong coordination reduces scalability and performance of the global system.

We introduce SporeDB as a way to solve these problems using simple (but powerful) techniques.

*To help the development effort, Bitcoin donations are welcome at `146VpSS56tkvt2ZSXLpwYBjjPR2grf5P9N`. Thanks!*

## Overview

The SporeDB architecture is represented in the following figure :
* SporeDB nodes are connected through a P2P network using a home-made "Mycelium" protocol ;
* Every node store a whole copy of the database, ensuring data security ;
* Write operations are applied by the cluster with the SporeDB Consensus Algorithm, presented (and hopefully proved) in the whitepaper ;
* Nodes expose a GRPC API server that can be used by clients to access the data and to submit transactions.

![overview](doc/overview.png)

## Installation

### With Go

```bash
$ go get gitlab.com/SporeDB/sporedb
```

### Without Go (Docker)

```bash
$ docker pull registry.gitlab.com/sporedb/sporedb
```

We suggest that you use docker volumes to preserve SporeDB states, like this:

```bash
$ docker run --rm -it -e PASSWORD=******* -v $PWD:/sporedb registry.gitlab.com/sporedb/sporedb --help
```

### Build from source

[Go 1.8+](https://golang.org/dl/) is required for source building:

```
$ make install-bolt
```

[RocksDB v5.6.x](https://github.com/facebook/rocksdb.git) is required to build SporeDB with RocksDB support (provides enhanced write performance):

```
$ make
```

Finally, some dependencies are required to build protobuf files:

* [Protoc v3.1.x](https://github.com/google/protobuf.git)
  * With the [Protoc-Gen-Go](https://github.com/golang/protobuf) plugin

**To setup a clean compilation environment please refer to the up-to-date [continuous integration](ci/Dockerfile) Dockerfile.**

## Configuration

### Writing main configuration file

SporeDB needs a YAML configuration file.
An example is available in [sporedb.yaml](sporedb.yaml) and can be used as-is after edition of `identity` field that will identify you in your network.

You might also want to add some information about peers to connect to in this configuration file.

*Please note that right now, network topology is mainly static. This will be upgraded to a full gossip network soon.*

### Setting-up crypto credentials

You will need some credentials to build a Trust Network.
SporeDB uses a system very similar to OpenPGP, with some specific modifications.
Basically, each node of the network holds a public/private Ed25519 keys pair for integrity verification.

First of all, you must set the `PASSWORD` environment variable.
This password will be used to encrypt your private key.
You might then want to send your public key to the other peers of your network.

```bash
$ export PASSWORD=********
$ sporedb keys init   # Will create your credentials
$ sporedb keys export # Will export your public key
```

You might also want to import other's public key with a specific trust level in your keyring.
For example, the following command imports the Alice's public key, stored in `alice.pem`, with a High trust level.

```
$ cat alice.pem > sporedb keys import alice -t high
```

For more information and advanced features (like key's signatures), see `sporedb keys -h`.

### Creating a policy

Policies define what nodes can and cannot do accross a network of nodes ("Mycelium").
We encourage you to read the [full whitepaper](https://static.lesterpig.com/sporedb.pdf) to fully understand how policy are designed.

Policies are stored in JSON files, and can be created with a wizzard:

```bash
$ sporedb keys ls
+----------+----------+-----------+----------------+
| Identity |  Trust   | Certified |  Fingerprint   |
+----------+----------+-----------+----------------+
| <self>   | ultimate | ✔️️ yes     | 63:29:41:4A:B9 |
+----------+----------+-----------+----------------+
| bob      | high     | ✔️️ yes     | B1:D3:CD:91:07 |
+----------+----------+-----------+----------------+
| carol    | high     | ✔️️ yes     | 09:0F:86:26:E0 |
+----------+----------+-----------+----------------+

$ sporedb policy create
Name of the policy [6cfeddad-eaec-4e0b-abf8-4658f0297402]:
Comment []: A test policy
Shall this node be considered as an endorser? [y/n] [y]: y
Endorser #1 (blank to skip) []: bob
Endorser #2 (blank to skip) []: carol
Endorser #3 (blank to skip) []:
Maximum number of byzantine (faulty) endorsers [1]: 1
Quorum [3]: 3
```

The previous dialog will create a policy that will allow the current node, bob and carol to endorse (validate) spore submissions in the network.

## Usage

Each SporeDB node will offer a GRPC API server, enabling Clients to connect to it.
Right now, it is possible to send basic instructions to one Node using embedded client.

On first terminal:

```bash
$ sporedb server
Successfully loaded policy test
SporeDB is running on localhost:4000
```

On second terminal:

```bash
$ sporedb client -s localhost:4000 -p test
Now using policy test
SporeDB client is connected and ready to execute your luscious instructions!
localhost:4000> SET key value
Transaction: bbd5aa6b-7b56-4ce9-926a-ea0ce6175ca0
localhost:4000> GET key
value
```

Documentation is being written about client capabilities.
You can though check the available commands [here](db/client/cli.go).

### Adding a systemd service

If you want to start your SporeDB automatically at each boot, you should consider creating a systemd service.

First, copy the service skeleton in the systemd configuration directory:

```bash
$ sudo cp ./sporedb.service /etc/systemd/system
```

Now you'll have to edit the file `/etc/systemd/system/sporedb.service` according to your installation.

Finally you can use systemd commands to manage your sporedb installation

```bash
$ sudo systemctl start sporedb       #--- start sporedb now
$ sudo systemctl status sporedb      #--- check sporedb status
$ sudo systemctl enable sporedb      #--- start sporedb on boot
$ sudo journalctl -u sporedb         #--- show sporedb logs
```

## Acknowledgements

SporeDB should **NOT** be used in production yet.
It is very new and not stable enough.

Feedbacks about the project and the whitepaper will be very much appreciated! 😘

## Implemented features

* Basic database management
  * SET operation
  * CONCAT operation
  * ADD operation (float)
  * MUL operation (float)
  * SADD operation (set)
  * SREM operation (set)
* Basic policy management
  * With disk usage quotas
* Integrity with Ed25519 signatures
  * CLI KeyRing management similar to OpenPGP
* P2P network
  * Gossip communications
  * Partial and Full State-Transfer support
* GRPC Server / Client API
* Pluggable underlying database drivers
  * BoltDB (default)
  * RocksDB
